package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BancoPreguntaDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer idBancoPregunta;
  private Integer codigoTipoDeclaracion;
  private String descripcionPregunta;
  private Integer codigoTipoPregunta;
  private Integer ordenApp;
  private Integer ordenFisicoSolicitud;
  private Integer ordenSubIndiceFisicoSolicitud;
  private Boolean aplicaPreguntaMujer;
  private Timestamp fechaCreacion;
  private Timestamp fechaModificacion;
  private Integer estadoSuscripcionInteligente;
  private Integer ordenSuscripcionInteligente;
}
