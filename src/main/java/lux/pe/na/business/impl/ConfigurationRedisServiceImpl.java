package lux.pe.na.business.impl;

import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.business.ConfigurationRedisService;
import lux.pe.na.constant.ConfigurationConstants;
import lux.pe.na.exception.ConfigurationNotFoundException;
import lux.pe.na.expose.response.ConfigurationResponse;
import lux.pe.na.model.dto.*;
import lux.pe.na.model.entity.CuentaValidacion;
import lux.pe.na.model.entity.ItemValor;
import lux.pe.na.model.entity.TipoCuenta;
import lux.pe.na.model.response.CentroMedicoResponse;
import lux.pe.na.model.response.EntidadResponse;
import lux.pe.na.model.response.ParentescoResponse;
import lux.pe.na.model.response.TablaMaestraResponse;
import lux.pe.na.redisclient.ConfigurationRedisClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.stream.Collectors;

import static lux.pe.na.constant.ConfigurationConstants.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConfigurationRedisServiceImpl implements ConfigurationRedisService {

  private final ConfigurationRedisClient configurationRedisClient;

  @Override
  public Mono<ConfigurationResponse> getTablaMaestraResponse(String id) {
    List<String> listId = new ArrayList<>();
    int tableId = 0;

    List<TablaMaestraDto> masterTableList = configurationRedisClient.masterTableDtoList();

    if (StringUtils.isNumeric(id)) {
      tableId = Integer.parseInt(id);
    }

    if (Strings.isNullOrEmpty(id) && !StringUtils.isNumeric(id)) {
      id = "001";
    }

    if (tableId > 0) {
      listId.add(String.valueOf(tableId));
    } else {
      listId = Arrays.stream(id.split(String.valueOf(','))).collect(Collectors.toList());
    }

    List<String> finalListId = listId;

    List<TablaMaestraResponse> masterTableResponseList = masterTableList.stream()
        .filter(tablaMaestraDto -> tablaMaestraDto.getId().equals(finalListId))
        .map(table -> TablaMaestraResponse.builder()
            .idTabla(table.getId())
            .id(table.getCodigoCampo())
            .tabla(table.getTabla())
            .valor(table.getValor())
            .build())
        .collect(Collectors.toList());

    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(masterTableResponseList)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getParentesco() {
    List<TablaMaestraDto> relationship = configurationRedisClient.masterTableDtoList()
        .stream()
        .filter(tablaMaestraDto -> tablaMaestraDto.getId()
            .equals(PARENTESCO))
        .collect(Collectors.toList());

    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(relationship.stream().map(ship -> ParentescoResponse.builder()
                .idTabla(ship.getId())
                .id(ship.getCodigoCampo())
                .tabla(ship.getTabla())
                .valor(ship.getValor())
                .codigoSexo(Integer.parseInt(ship.getValorAuxiliar()))
                .aplicaFondoUniversitario(Boolean.getBoolean(ship.getEquivalenciaViap()))
                .build())
            .collect(Collectors.toList()))
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getOperadores(Integer collectionTypeId) {
    List<EntidadDto> operationsEntity;

    if (collectionTypeId != 0) {
      operationsEntity = configurationRedisClient.entityDtoList()
          .stream()
          .filter(entity -> entity.getCodigoTipoCobranza().equals(collectionTypeId))
          .collect(Collectors.toList());
    } else {
      operationsEntity = configurationRedisClient.entityDtoList();
    }

    List<EntidadResponse> entityResponseList = operationsEntity.stream()
        .map(entidadDto -> EntidadResponse.builder()
            .id(entidadDto.getCodigoEntidad())
            .valor(entidadDto.getDescripcion())
            .visaClickApp(entidadDto.getCodigoEntidadVisaClickApp())
            .tipoCuentas(configurationRedisClient.entityAccountTypeDtoList()
                .stream()
                .filter(tipoCuentaEntidadDto -> tipoCuentaEntidadDto.getCodigoEntidad().equals(entidadDto.getCodigoEntidad()))
                .map(tipoCuenta -> TipoCuenta.builder()
                    .id(tipoCuenta.getCodigoTipoCuenta())
                    .descripcion(tipoCuenta.getDescripcion())
                    .plantillaNumeroCuenta(tipoCuenta.getPlantillaNumeroCuenta())
                    .moneda(ItemValor.builder()
                        .codigo(tipoCuenta.getCodigoMoneda())
                        .descripcion(configurationRedisClient.masterTableDtoList()
                            .stream()
                            .filter(tablaMaestraDto -> tablaMaestraDto.getId().equals(1))
                            .filter(tablaMaestra -> tablaMaestra.getCodigoCampo().equals(tipoCuenta.getCodigoMoneda()))
                            .map(TablaMaestraDto::getValor)
                            .findFirst()
                            .orElse(""))
                        .build())
                    .validaciones(configurationRedisClient.validationEntityAccountDtoList().stream()
                        .filter(validation -> validation.getCodigoEntidad().equals(tipoCuenta.getCodigoEntidad())
                            && validation.getCodigoTipoCuenta().equals(tipoCuenta.getCodigoTipoCuenta()))
                        .map(cuentaEntidadValidacionDto -> CuentaValidacion.builder()
                            .posicionInicial(cuentaEntidadValidacionDto.getPosicionInicial())
                            .posicionFinal(cuentaEntidadValidacionDto.getPosicionFinal())
                            .caracterValidador(cuentaEntidadValidacionDto.getCaracterValidador())
                            .descripcion(cuentaEntidadValidacionDto.getDescripcion())
                            .build())
                        .collect(Collectors.toList()))
                    .build())
                .collect(Collectors.toList())
            )
            .build())
        .collect(Collectors.toList());

    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(entityResponseList)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getUbigeo(Integer departmentCode, Integer provinceCode, Integer districtCode) {
    List<ItemValor> ubigeo = null;

    if (departmentCode != 0 && provinceCode < 0 && districtCode < 0) {
      ubigeo = getDepartment(departmentCode);
    } else if (departmentCode != 0 && provinceCode != 0 && districtCode < 0) {
      ubigeo = getProvince(departmentCode, provinceCode);
    } else if (departmentCode != 0 && provinceCode < 0 && districtCode != 0) {
      ubigeo = getDistrict(departmentCode, provinceCode, districtCode);
    }
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(ubigeo)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getMedicalCenter(Integer departmentCode, Integer provinceCode, Integer districtCode) {
    List<CentroMedicoDto> medicalCenter;
    medicalCenter = configurationRedisClient.medicalCenterDtoList();

    if (departmentCode != 0) {
      medicalCenter = medicalCenter
          .stream()
          .filter(department -> department.getCodigoDepartamento().equals(departmentCode))
          .collect(Collectors.toList());
    }

    if (provinceCode != 0) {
      medicalCenter = medicalCenter
          .stream()
          .filter(province -> province.getCodigoProvincia().equals(provinceCode))
          .collect(Collectors.toList());
    }

    if (districtCode != 0) {
      medicalCenter = medicalCenter
          .stream()
          .filter(district -> district.getCodigoDistrito().equals(districtCode))
          .collect(Collectors.toList());
    }

    List<CentroMedicoResponse> medicalCenterList = medicalCenter.stream()
        .map(medical -> CentroMedicoResponse.builder()
            .id(medical.getCodigoCentroMedico())
            .descripcion(medical.getDescripcion())
            .direccion(medical.getDireccion())
            .atencionDomicilio(medical.getFlagAtencionDomicilio())
            .departamento(ItemValor.builder()
                .codigo(medical.getCodigoDepartamento())
                .descripcion(getDepartment(medical.getCodigoDepartamento())
                    .stream()
                    .map(ItemValor::getDescripcion)
                    .findFirst().orElse(""))
                .build())
            .provincia(ItemValor.builder()
                .codigo(medical.getCodigoProvincia())
                .descripcion(getProvince(medical.getCodigoDepartamento(), medical.getCodigoProvincia())
                    .stream()
                    .map(ItemValor::getDescripcion)
                    .findFirst().orElse(""))
                .build())
            .distrito(ItemValor.builder()
                .codigo(medical.getCodigoDistrito())
                .descripcion(getDistrict(medical.getCodigoDepartamento(), medical.getCodigoProvincia(), medical.getCodigoDistrito())
                    .stream()
                    .map(ItemValor::getDescripcion)
                    .findFirst().orElse(""))
                .build())
            .build())
        .collect(Collectors.toList());

    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(medicalCenterList)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getMedicalCenterId(Integer id) {
    CentroMedicoResponse medicalCenterResponse = configurationRedisClient.medicalCenterDtoList()
        .stream()
        .filter(centroMedicoDto -> centroMedicoDto.getCodigoCentroMedico().equals(id))
        .map(medical -> CentroMedicoResponse.builder()
            .id(medical.getCodigoCentroMedico())
            .descripcion(medical.getDescripcion())
            .direccion(medical.getDireccion())
            .atencionDomicilio(medical.getFlagAtencionDomicilio())
            .departamento(ItemValor.builder()
                .codigo(medical.getCodigoDepartamento())
                .descripcion(getDepartment(medical.getCodigoDepartamento())
                    .stream()
                    .map(ItemValor::getDescripcion)
                    .findFirst()
                    .orElseThrow())
                .build())
            .provincia(ItemValor.builder()
                .codigo(medical.getCodigoProvincia())
                .descripcion(getProvince(medical.getCodigoDepartamento(), medical.getCodigoProvincia())
                    .stream()
                    .map(ItemValor::getDescripcion)
                    .findFirst()
                    .orElseThrow())
                .build())
            .distrito(ItemValor.builder()
                .codigo(medical.getCodigoDistrito())
                .descripcion(getDistrict(medical.getCodigoDepartamento(), medical.getCodigoProvincia(), medical.getCodigoDistrito())
                    .stream()
                    .map(ItemValor::getDescripcion)
                    .findFirst()
                    .orElseThrow())
                .build())
            .build())
        .findFirst()
        .orElseThrow();

    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(medicalCenterResponse)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllBankAsk() {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.bankAskDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllInvestmentFunds() {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.investmentFundsDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllSumInsured() {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.sumInsuredDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllPackage(Integer packageCode) {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.packageDtoList()
            .stream()
            .filter(paqueteDto -> paqueteDto.getCodigoPaquete()
                .equals(packageCode))
            .collect(Collectors.toList()))
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getProductVersion() {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.productVersionDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getProductVersionCode(String productCode, String codeVersionProduct) {

    if (productCode.equals("189")) {
      productCode = "198";
    }

    String finalProductCode = productCode;
    List<ProductoVersionDto> productVersionList = configurationRedisClient.productVersionDtoList().stream()
        .filter(product -> product.getCodigoProducto().equals(finalProductCode)
            && product.getCodigoVersionProducto().equals(codeVersionProduct))
        .collect(Collectors.toList());
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(productVersionList)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getProductCoverage() {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.productCoverageDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getProductCoverageCode(String productCode, String codeVersionProduct) {
    List<CoberturaProductoDto> productCoverageList = configurationRedisClient.productCoverageDtoList().stream()
        .filter(coverage -> coverage.getCodigoProducto().equals(productCode)
            && coverage.getCodigoVersionProducto().equals(codeVersionProduct))
        .collect(Collectors.toList());
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(productCoverageList)
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getProductReturn() {
    return Mono.just(ConfigurationResponse.builder()
        .code(CODE_SUCCESS)
        .status(STATUS_SUCCESS)
        .data(configurationRedisClient.productReturnDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getProductReturnCode(String productCode, String codeVersionProduct) {
    List<RetornoProductoDto> productReturnList = configurationRedisClient.productReturnDtoList().stream()
        .filter(product -> product.getCodigoProducto()
            .equals(productCode)
            && product.getCodigoVersionProducto()
            .equals(codeVersionProduct))
        .collect(Collectors.toList());
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(productReturnList)
        .build());
  }

  private List<ItemValor> getDepartment(Integer departmentId) {
    List<UbigeoDto> departments;

    departments = configurationRedisClient.ubigeoDtoList();

    if (departmentId != 0) {
      departments = departments.stream()
          .filter(department -> department.getCodigoDepartamento().equals(departmentId))
          .collect(Collectors.toList());
    }

    return departments.stream()
        .map(department -> ItemValor.builder()
            .codigo(department.getCodigoDepartamento())
            .descripcion(department.getDepartamento())
            .build())
        .distinct()
        .collect(Collectors.toList());
  }

  private List<ItemValor> getProvince(Integer departmentId, Integer provinceId) {
    List<UbigeoDto> provinces;

    provinces = configurationRedisClient.ubigeoDtoList();

    if (departmentId != 0) {
      provinces = provinces.stream()
          .filter(department -> department.getCodigoDepartamento().equals(departmentId))
          .collect(Collectors.toList());
    }

    if (provinceId != 0) {
      provinces = provinces.stream()
          .filter(province -> province.getCodigoProvincia().equals(provinceId))
          .collect(Collectors.toList());
    }

    return provinces.stream()
        .map(province -> ItemValor.builder()
            .codigo(province.getCodigoProvincia())
            .descripcion(province.getProvincia())
            .build())
        .distinct()
        .collect(Collectors.toList());

  }

  private List<ItemValor> getDistrict(Integer departmentId, Integer provinceId, Integer districtId) {
    List<UbigeoDto> districts;

    districts = configurationRedisClient.ubigeoDtoList();

    if (departmentId > 0) {
      districts = districts.stream()
          .filter(department -> department.getCodigoDepartamento().equals(departmentId))
          .collect(Collectors.toList());
    }

    if (provinceId != 0) {
      districts = districts.stream()
          .filter(province -> province.getCodigoProvincia().equals(provinceId))
          .collect(Collectors.toList());
    }

    if (districtId != 0) {
      districts = districts.stream()
          .filter(district -> district.getCodigoDistrito().equals(districtId))
          .collect(Collectors.toList());
    }

    return districts.stream()
        .map(province -> ItemValor.builder()
            .codigo(province.getCodigoDistrito())
            .descripcion(province.getDistrito())
            .build())
        .distinct()
        .collect(Collectors.toList());

  }

  /*@Override
  public Mono<ConfigurationResponse> getAllBankAsk() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.bankAskDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getParameterById(Integer id) {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.parameterDtoList()
            .stream()
            .filter(parametroDto -> parametroDto.getIdParametro().equals(id))
            .findFirst()
            .orElseThrow(() -> new
                ConfigurationNotFoundException("No se encontró el Parametro con el ID "
                .concat(String.valueOf(id)))))
        .build());
  } // TODO ESTA OK

  @Override
  public Mono<ConfigurationResponse> getAllMedicalCenter() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.medicalCenterDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getMedicalCenterById(Integer id) {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.medicalCenterDtoList()
            .stream()
            .filter(parametroDto -> parametroDto.getCodigoCentroMedico().equals(id))
            .findFirst()
            .orElseThrow(() -> new
                ConfigurationNotFoundException("No se encontró el Centro Medico con el ID "
                .concat(String.valueOf(id)))))
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllUbigeo() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.ubigeoDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllMasterTable() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.masterTableDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getMasterTableById(Integer id) {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.masterTableDtoList()
            .stream()
            .filter(tablaMaestraDto -> tablaMaestraDto.getId().equals(id))
            .collect(Collectors.toList()))
        .build());
  } // TODO ESTA OK

  @Override
  public Mono<ConfigurationResponse> getMasterTableByIdAndFielCode(Integer id, Integer fielCode) {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.masterTableDtoList()
            .stream()
            .filter(tablaMaestraDto -> tablaMaestraDto.getId().equals(id)
                && tablaMaestraDto.getCodigoCampo().equals(fielCode))
            .findFirst()
            .orElseThrow(() ->
                new ConfigurationNotFoundException("No se encontró el Centro Medico con el ID "
                    .concat(String.valueOf(id)))))
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllEntity() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.entityDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllEntityAccountType() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.entityAccountTypeDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllValidationEntityAccount() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.validationEntityAccountDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllProductCoverage() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.productCoverageDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllInvestmentFunds() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.investmentFundsDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllPackage() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.packageDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllProductVersion() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.productVersionDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllProductReturn() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.productReturnDtoList())
        .build());
  }

  @Override
  public Mono<ConfigurationResponse> getAllSumInsured() {
    return Mono.just(ConfigurationResponse.builder()
        .code(200)
        .status("success")
        .data(configurationRedisClient.sumInsuredDtoList())
        .build());
  }*/

}
