package lux.pe.na.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lux.pe.na.model.entity.TipoCuenta;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EntidadResponse {

  private Integer id;
  private String valor;
  private String visaClickApp;
  private List<TipoCuenta> tipoCuentas;
}
