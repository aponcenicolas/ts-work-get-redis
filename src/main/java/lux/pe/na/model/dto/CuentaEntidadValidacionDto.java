package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CuentaEntidadValidacionDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer idCuentaEntidadValidacion;
  private Integer codigoValidacion;
  private Integer codigoEntidad;
  private Integer codigoTipoCuenta;
  private Integer posicionInicial;
  private Integer posicionFinal;
  private String caracterValidador;
  private String descripcion;
  private Boolean flagActivo;
  private Timestamp fechaCreacion;
  private Timestamp fechaModificacion;
}
