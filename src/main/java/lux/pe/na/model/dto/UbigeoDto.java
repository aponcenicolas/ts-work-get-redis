package lux.pe.na.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UbigeoDto implements Serializable {

  private Integer codigoDistrito;
  private Integer codigoProvincia;
  private Integer codigoDepartamento;
  private String distrito;
  private String provincia;
  private String departamento;

}
