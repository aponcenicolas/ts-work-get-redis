package lux.pe.na.redisclient;

import lux.pe.na.model.dto.*;

import java.util.List;

public interface ConfigurationRedisClient {

  List<BancoPreguntaDto> bankAskDtoList();

  List<ParametroDto> parameterDtoList();

  List<CentroMedicoDto> medicalCenterDtoList();

  List<UbigeoDto> ubigeoDtoList();

  List<TablaMaestraDto> masterTableDtoList();

  List<EntidadDto> entityDtoList();

  List<TipoCuentaEntidadDto> entityAccountTypeDtoList();

  List<CuentaEntidadValidacionDto> validationEntityAccountDtoList();

  List<CoberturaProductoDto> productCoverageDtoList();

  List<FondoInversionDto> investmentFundsDtoList();

  List<PaqueteDto> packageDtoList();

  List<ProductoVersionDto> productVersionDtoList();

  List<RetornoProductoDto> productReturnDtoList();

  List<SumaAseguradaDto> sumInsuredDtoList();


}
