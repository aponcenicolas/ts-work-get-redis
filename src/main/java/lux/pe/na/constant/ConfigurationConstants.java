package lux.pe.na.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConfigurationConstants {

  public static final Integer PARENTESCO = 36;
  public static final Integer CODE_SUCCESS = 200;
  public static final String STATUS_SUCCESS = "success";
}
