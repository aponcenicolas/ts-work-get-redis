package lux.pe.na.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CuentaValidacion {

  private Integer posicionInicial;
  private Integer posicionFinal;
  private String caracterValidador;
  private String descripcion;
}
