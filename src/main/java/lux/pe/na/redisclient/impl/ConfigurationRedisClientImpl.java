package lux.pe.na.redisclient.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.model.dto.*;
import lux.pe.na.redisclient.ConfigurationRedisClient;
import lux.pe.na.redisclient.config.RedisKeyNames;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class ConfigurationRedisClientImpl implements ConfigurationRedisClient {

  private final StringRedisTemplate redisTemplate;
  private ValueOperations<String, String> valueOperations;

  @PostConstruct
  private void init() {
    valueOperations = redisTemplate.opsForValue();
  }

  @Override
  public List<BancoPreguntaDto> bankAskDtoList() {
    return getListFromRedis(RedisKeyNames.BANCO_PREGUNTA.getKey(), new TypeReference<List<BancoPreguntaDto>>() {
    });
  }

  @Override
  public List<ParametroDto> parameterDtoList() {
    return getListFromRedis(RedisKeyNames.PARAMETRO.getKey(), new TypeReference<List<ParametroDto>>() {
    });
  }

  @Override
  public List<CentroMedicoDto> medicalCenterDtoList() {
    return getListFromRedis(RedisKeyNames.CENTRO_MEDICO.getKey(), new TypeReference<List<CentroMedicoDto>>() {
    });
  }

  @Override
  public List<UbigeoDto> ubigeoDtoList() {
    return getListFromRedis(RedisKeyNames.UBIGEO.getKey(), new TypeReference<List<UbigeoDto>>() {
    });
  }

  @Override
  public List<TablaMaestraDto> masterTableDtoList() {
    return getListFromRedis(RedisKeyNames.TABLA_MAESTRA.getKey(), new TypeReference<List<TablaMaestraDto>>() {
    });
  }

  @Override
  public List<EntidadDto> entityDtoList() {
    return getListFromRedis(RedisKeyNames.ENTIDAD.getKey(), new TypeReference<List<EntidadDto>>() {
    });
  }

  @Override
  public List<TipoCuentaEntidadDto> entityAccountTypeDtoList() {
    return getListFromRedis(RedisKeyNames.TIPO_CUENTA_ENTIDAD.getKey(), new TypeReference<List<TipoCuentaEntidadDto>>() {
    });
  }

  @Override
  public List<CuentaEntidadValidacionDto> validationEntityAccountDtoList() {
    return getListFromRedis(RedisKeyNames.CUENTA_ENTIDAD_VALIDACION.getKey(), new TypeReference<List<CuentaEntidadValidacionDto>>() {
    });
  }

  @Override
  public List<CoberturaProductoDto> productCoverageDtoList() {
    return getListFromRedis(RedisKeyNames.COBERTURA_PRODUCTO.getKey(), new TypeReference<List<CoberturaProductoDto>>() {
    });
  }

  @Override
  public List<FondoInversionDto> investmentFundsDtoList() {
    return getListFromRedis(RedisKeyNames.FONDOS_INVERSION.getKey(), new TypeReference<List<FondoInversionDto>>() {
    });
  }

  @Override
  public List<PaqueteDto> packageDtoList() {
    return getListFromRedis(RedisKeyNames.PAQUETES.getKey(), new TypeReference<List<PaqueteDto>>() {
    });
  }

  @Override
  public List<ProductoVersionDto> productVersionDtoList() {
    return getListFromRedis(RedisKeyNames.PRODUCTOS_VERSIONES.getKey(), new TypeReference<List<ProductoVersionDto>>() {
    });
  }

  @Override
  public List<RetornoProductoDto> productReturnDtoList() {
    return getListFromRedis(RedisKeyNames.RETORNO_PRODUCTOS.getKey(), new TypeReference<List<RetornoProductoDto>>() {
    });
  }

  @Override
  public List<SumaAseguradaDto> sumInsuredDtoList() {
    return getListFromRedis(RedisKeyNames.SUMAS_ASEGURADAS.getKey(), new TypeReference<List<SumaAseguradaDto>>() {
    });
  }


  private <T> List<T> getListFromRedis(String keyName, TypeReference<List<T>> typeReference) {
    if (Boolean.TRUE.equals(redisTemplate.hasKey(keyName))) {
      String jsonValue = valueOperations.get(keyName);

      try {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonValue, typeReference);
      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
    }
    return Collections.emptyList();
  }
}
