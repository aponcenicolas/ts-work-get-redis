package lux.pe.na.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lux.pe.na.model.entity.ItemValor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CentroMedicoResponse {

  private Integer id;
  private String descripcion;
  private String direccion;
  private boolean atencionDomicilio;
  private ItemValor departamento;
  private ItemValor provincia;
  private ItemValor distrito;
}
