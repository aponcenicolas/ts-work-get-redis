package lux.pe.na.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TipoCuenta {

  private Integer id;
  private String descripcion;
  private String plantillaNumeroCuenta;
  private ItemValor moneda;
  private List<CuentaValidacion> validaciones;

}
