package lux.pe.na.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TablaMaestraResponse {

  private Integer id;
  private String tabla;
  private Integer idTabla;
  private String valor;
}
