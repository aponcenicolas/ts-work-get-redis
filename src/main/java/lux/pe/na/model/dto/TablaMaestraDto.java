package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TablaMaestraDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;
  private String tabla;
  private Integer codigoCampo;
  private String valor;
  private Double valorNumerico;
  private String valorAuxiliar;
  private String equivalenciaViap;
}
