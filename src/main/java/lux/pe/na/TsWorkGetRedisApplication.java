package lux.pe.na;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TsWorkGetRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsWorkGetRedisApplication.class, args);
	}

}
