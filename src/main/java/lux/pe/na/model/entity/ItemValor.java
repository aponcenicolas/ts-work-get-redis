package lux.pe.na.model.entity;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class ItemValor {

  private Integer codigo;
  private String descripcion;
}
