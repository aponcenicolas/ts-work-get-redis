package lux.pe.na.expose.response;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfigurationResponse implements Serializable {

  private Integer code;
  private String status;
  private Object data;
}
