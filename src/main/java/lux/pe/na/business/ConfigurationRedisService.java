package lux.pe.na.business;

import lux.pe.na.expose.response.ConfigurationResponse;
import lux.pe.na.model.*;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ConfigurationRedisService {

  Mono<ConfigurationResponse> getTablaMaestraResponse(String id);

  Mono<ConfigurationResponse> getParentesco();

  Mono<ConfigurationResponse> getOperadores(Integer collectionTypeId);

  Mono<ConfigurationResponse> getUbigeo(Integer departmentCode, Integer provinceCode, Integer districtCode);

  Mono<ConfigurationResponse> getMedicalCenter(Integer departmentCode, Integer provinceCode, Integer districtCode);

  Mono<ConfigurationResponse> getMedicalCenterId(Integer id);

  Mono<ConfigurationResponse> getAllBankAsk();

  Mono<ConfigurationResponse> getAllInvestmentFunds();

  Mono<ConfigurationResponse> getAllSumInsured();

  Mono<ConfigurationResponse> getAllPackage(Integer packageCode);

  Mono<ConfigurationResponse> getProductVersion();

  Mono<ConfigurationResponse> getProductVersionCode(String productCode, String codeVersionProduct);

  Mono<ConfigurationResponse> getProductCoverage();

  Mono<ConfigurationResponse> getProductCoverageCode(String productCode, String codeVersionProduct);

  Mono<ConfigurationResponse> getProductReturn();

  Mono<ConfigurationResponse> getProductReturnCode(String productCode, String codeVersionProduct);

  /*Mono<ConfigurationResponse> getAllBankAsk();

  Mono<ConfigurationResponse> getParameterById(Integer id);

  Mono<ConfigurationResponse> getAllMedicalCenter();

  Mono<ConfigurationResponse> getMedicalCenterById(Integer id);

  Mono<ConfigurationResponse> getAllUbigeo();

  Mono<ConfigurationResponse> getAllMasterTable();

  Mono<ConfigurationResponse> getMasterTableById(Integer id);

  Mono<ConfigurationResponse> getMasterTableByIdAndFielCode(Integer id, Integer fielCode);

  Mono<ConfigurationResponse> getAllEntity();

  Mono<ConfigurationResponse> getAllEntityAccountType();

  Mono<ConfigurationResponse> getAllValidationEntityAccount();

  Mono<ConfigurationResponse> getAllProductCoverage();

  Mono<ConfigurationResponse> getAllInvestmentFunds();

  Mono<ConfigurationResponse> getAllPackage();

  Mono<ConfigurationResponse> getAllProductVersion();

  Mono<ConfigurationResponse> getAllProductReturn();

  Mono<ConfigurationResponse> getAllSumInsured();*/


}
