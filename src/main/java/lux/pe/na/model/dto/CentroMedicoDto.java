package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CentroMedicoDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer codigoCentroMedico;
  private String descripcion;
  private String direccion;
  private Integer codigoDepartamento;
  private Integer codigoProvincia;
  private Integer codigoDistrito;
  private Boolean flagAtencionDomicilio;
  private Boolean flagActivo;
  private Timestamp fechaCreacion;
  private Timestamp fechaModificacion;
}
