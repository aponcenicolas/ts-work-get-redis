package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EntidadDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer codigoEntidad;
  private String descripcion;
  private Integer codigoTipoCobranza;
  private Timestamp fechaCreacion;
  private Timestamp fechaModificacion;
  private String codigoEntidadPps;
  private String codigoTarjetaPps;
  private Boolean flagSincronizableSalud;
  private Boolean aplicaEps;
  private Boolean flagAplicaEps;
  private Boolean flagActivo;
  private String numIdRelacionIngresoPps;
  private String codigoEntidadVisaClickApp;
}
