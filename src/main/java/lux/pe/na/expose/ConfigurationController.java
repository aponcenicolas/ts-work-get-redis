package lux.pe.na.expose;

import lombok.AllArgsConstructor;
import lux.pe.na.business.ConfigurationRedisService;
import lux.pe.na.constant.ConfigurationConstants;
import lux.pe.na.expose.response.ConfigurationResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class ConfigurationController {

  private final ConfigurationRedisService configurationRedisService;


  @GetMapping("/configuraciones/tablamaestras/{id}")
  public Mono<ResponseEntity<ConfigurationResponse>> getMasterTableById(@PathVariable Integer id) {
    if (id.equals(ConfigurationConstants.PARENTESCO)) {
      return configurationRedisService.getParentesco()
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    } else {
      return configurationRedisService.getTablaMaestraResponse(String.valueOf(id))
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }
  }

  @GetMapping("/configuraciones/tablamaestras")
  public Mono<ResponseEntity<ConfigurationResponse>> getMasterTable(@RequestParam String id) {
    return configurationRedisService.getTablaMaestraResponse(id)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());

  }

  @GetMapping("/configuraciones/operadores")
  public Mono<ResponseEntity<ConfigurationResponse>> getOperations(@RequestParam Integer collectionTypeId) {
    return configurationRedisService.getOperadores(collectionTypeId)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());

  }

  @GetMapping("/configuraciones/ubigeo")
  public Mono<ResponseEntity<ConfigurationResponse>> getUbigeo(@RequestParam Integer department,
                                                               @RequestParam Integer province, @RequestParam Integer district) {
    return configurationRedisService.getUbigeo(department, province, district)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());

  }

  @GetMapping("/configuraciones/centrosmedicos")
  public Mono<ResponseEntity<ConfigurationResponse>> getMedicalCenter(@RequestParam Integer department,
                                                                      @RequestParam Integer province, @RequestParam Integer district) {
    return configurationRedisService.getMedicalCenter(department, province, district)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());

  }

  @GetMapping("/configuraciones/centrosmedicos/{id}")
  public Mono<ResponseEntity<ConfigurationResponse>> getMedicalCenterById(@PathVariable Integer id) {
    return configurationRedisService.getMedicalCenterId(id)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());

  }

  @GetMapping("/configuraciones/bancopreguntas")
  public Mono<ResponseEntity<ConfigurationResponse>> getBankAsk() {
    return configurationRedisService.getAllBankAsk()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());

  }

  @GetMapping("/configuraciones/fondosinversion")
  public Mono<ResponseEntity<ConfigurationResponse>> getInversionFounds() {
    return configurationRedisService.getAllInvestmentFunds()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/sumasaseguradas")
  public Mono<ResponseEntity<ConfigurationResponse>> getSumInsured() {
    return configurationRedisService.getAllSumInsured()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/paquetes/{codigoPaquete}")
  public Mono<ResponseEntity<ConfigurationResponse>> getSumInsured(@PathVariable(value = "codigoPaquete") Integer packageCode) {
    return configurationRedisService.getAllPackage(packageCode)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/productos/{codigoProducto}/{codigoVersionProducto}/versiones")
  public Mono<ResponseEntity<ConfigurationResponse>> getProductVersion(@PathVariable(value = "codigoProducto") String productCode,
                                                                       @PathVariable(value = "codigoVersionProducto") String codeVersionProduct) {
    if (StringUtils.isNoneEmpty(productCode) && StringUtils.isNoneEmpty(codeVersionProduct)) {
      return configurationRedisService.getProductVersionCode(productCode, codeVersionProduct)
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    } else {
      return configurationRedisService.getProductVersion()
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }
  }

  @GetMapping("/configuraciones/productos/{codigoProducto}/{codigoVersionProducto}/coberturas")
  public Mono<ResponseEntity<ConfigurationResponse>> getProductCoverage(@PathVariable(value = "codigoProducto") String productCode,
                                                                        @PathVariable(value = "codigoVersionProducto") String codeVersionProduct) {
    if (StringUtils.isNoneEmpty(productCode) && StringUtils.isNoneEmpty(codeVersionProduct)) {
      return configurationRedisService.getProductCoverageCode(productCode, codeVersionProduct)
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    } else {
      return configurationRedisService.getProductCoverage()
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }
  }

  @GetMapping("/configuraciones/productos/{codigoProducto}/{codigoVersionProducto}/retornos")
  public Mono<ResponseEntity<ConfigurationResponse>> getProductReturn(@PathVariable(value = "codigoProducto") String productCode,
                                                                      @PathVariable(value = "codigoVersionProducto") String codeVersionProduct) {
    if (StringUtils.isNoneEmpty(productCode) && StringUtils.isNoneEmpty(codeVersionProduct)) {
      return configurationRedisService.getProductReturnCode(productCode, codeVersionProduct)
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    } else {
      return configurationRedisService.getProductReturn()
          .map(configurationResponse ->
              ResponseEntity.status(HttpStatus.OK)
                  .body(configurationResponse))
          .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }
  }


  /*
  @GetMapping("/configuraciones/bancopreguntas")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllBankAsk() {
    return configurationRedisService.getAllBankAsk()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/parametros/{id}")
  public Mono<ResponseEntity<ConfigurationResponse>> getParameterById(
      @PathVariable Integer id) {
    return configurationRedisService.getParameterById(id)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
  }

  @GetMapping("/configuraciones/centrosmedicos")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllMedicalCenter() {
    return configurationRedisService.getAllMedicalCenter()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/centrosmedicos/{id}")
  public Mono<ResponseEntity<ConfigurationResponse>> getMedicalCenterById(
      @PathVariable Integer id) {
    return configurationRedisService.getMedicalCenterById(id)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
  }

  @GetMapping("/configuraciones/ubigeo")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllUbigeo() {
    return configurationRedisService.getAllUbigeo()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/tablasmaestras")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllMasterTable() {
    return configurationRedisService.getAllMasterTable()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/tablasmaestras/{id}")
  public Mono<ResponseEntity<ConfigurationResponse>> getMasterTableById(
      @PathVariable Integer id) {
    return configurationRedisService.getMasterTableById(id)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/tablasmaestras/{id}/{codigocampo}")
  public Mono<ResponseEntity<ConfigurationResponse>> getMasterTableByIdAndFielCode(@PathVariable Integer id,
                                                                                   @PathVariable(value = "codigocampo") Integer fielCode) {
    return configurationRedisService.getMasterTableByIdAndFielCode(id, fielCode)
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
  }

  @GetMapping("/configuraciones/entidades")

  public Mono<ResponseEntity<ConfigurationResponse>> getAllEntity() {
    return configurationRedisService.getAllEntity()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/tipocuentaentidades")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllEntityAccountType() {
    return configurationRedisService.getAllEntityAccountType()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/cuentaentidadvalidaciones")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllValidationEntityAccount() {
    return configurationRedisService.getAllValidationEntityAccount()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/coberturaproductos")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllProductCoverage() {
    return configurationRedisService.getAllProductCoverage()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/fondosinversion")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllInvestmentFunds() {
    return configurationRedisService.getAllInvestmentFunds()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/paquetes")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllPackage() {
    return configurationRedisService.getAllPackage()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/productosversiones")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllProductVersion() {
    return configurationRedisService.getAllProductVersion()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/retornoproductos")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllProductReturn() {
    return configurationRedisService.getAllProductReturn()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping("/configuraciones/sumasaseguradas")
  public Mono<ResponseEntity<ConfigurationResponse>> getAllSumInsured() {
    return configurationRedisService.getAllSumInsured()
        .map(configurationResponse ->
            ResponseEntity.status(HttpStatus.OK)
                .body(configurationResponse))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }
*/
}
