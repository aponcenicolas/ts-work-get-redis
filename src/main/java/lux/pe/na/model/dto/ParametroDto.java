package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParametroDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer idParametro;
  private String descripcion;
  private String valorCadena;
  private Double valorNumerico;
}
