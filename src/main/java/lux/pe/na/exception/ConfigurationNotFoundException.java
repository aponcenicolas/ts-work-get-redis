package lux.pe.na.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ConfigurationNotFoundException extends RuntimeException {

  private String message;
}
