package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TipoCuentaEntidadDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer idTipoCuentaEntidad;
  private Integer codigoEntidad;
  private Integer codigoTipoCuenta;
  private String descripcion;
  private String plantillaNumeroCuenta;
  private Integer codigoMoneda;
  private Timestamp fechaCreacion;
  private Timestamp fechaModificacion;
  private String codigoTipoCuentaPps;
  private Boolean flagSincronizableSalud;
  private Boolean flagActivoSalud;
}
